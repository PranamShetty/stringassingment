function fullname(obj) {
  let fullName = "";
  if (obj["first_name"]) {
    fullName = obj["first_name"] + " ";
  }
  if (obj["middle_name"]) {
    fullName += obj["middle_name"] + " ";
  }
  if (obj["last_name"]) {
    fullName += obj["last_name"];
  }
  return fullName;
}

module.exports = fullname

